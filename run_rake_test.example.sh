#!/bin/bash
set -e

# This is an example script that sets up the remote server information
# needed to run the full suite of tests.  Actual values have been
# replaced with nonsense.
#
# If none of these are set, only offline tests will be run.
#
# Put your test command on the command line to invoke it; otherwise,
# it will run `rake test`.


#
# Set these to enable basic read tests:
#

# The host
#export SHEP_TEST_HOST="mastodon.anti.social"

# The public account we read stuff from:
#export SHEP_READ_ACCOUNT_HANDLE="@somerandombot"
#export SHEP_READ_ACCOUNT=69420


#
# Set these if you also have an account that you wish to use for
# testing.  This will only read from the account (we hope), but it's
# probably not a good idea to use an account you care about.
#

#export SHEP_ACCOUNT=54321
#export SHEP_TOKEN=JDJidvpsdvjapojSDFvfvafWER3qrwDVe4


#
# Set this if you want the tests to also write to the account.  If
# they run successfully, they should have deleted every status they
# create and no others.
#
# Note that deletions and media uploads (both things these tests do)
# are heavily rate-limited so we turn this test off by default.
#

#export SHEP_TEST_ALL=1

cd $(dirname "$0")/shep

if [ -z "$1" ]; then
    rake test
else
    eval "$@"
fi
