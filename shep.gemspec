require_relative "lib/shep/version"

Gem::Specification.new do |s|
  s.name        = 'shep'
  s.version     = Shep::Version
  s.date        = '2023-06-03'
  s.summary     = "An alternate interface to the Mastodon Web API"
  s.description = <<-EOF
Shep is a library that provides access to the Mastodon Web API.  It
has no gem dependencies beyond the base Ruby (3.0.x) installation and
can be tested without a dedicated Mastodon server.  It is intended as
a slightly more feature-rich alternative to the mastodon-api gem.
EOF
  s.authors     = ["Chris Reuter"]
  s.email       = 'chris@isplitonyourgrave.com'

  # I'm just going to add everything so that if you've got the gem,
  # you've also got the source distribution.  Yay! Open source!
  s.files = Dir.glob('doc/**/*') + `git ls-files`.split(/\n/)

  s.required_ruby_version = '>= 3.0.0'
  s.requirements << "A Ruby implementing 3.0.x syntax and the standard libs."

  s.add_development_dependency "rspec", '~> 3.12.0'
  s.add_development_dependency "yard", '~> 0.9.34'
  s.add_development_dependency 'rake', '~> 13.0.6'
  
  s.homepage    = 'https://codeberg.org/suetanvil/shep'
  s.license     = 'AGPL-3.0-only with linking exception'
end
