# Shep: Simple Http intErface to Pmastodon

Shep is a library that provides access to the Mastodon web API.  It is
an alternative to the
[official Mastodon API Gem](https://rubygems.org/gems/mastodon-api/versions/2.0.0).

Pros:

* It has no dependencies other than stock Ruby 3.0.x or later.  (It's
  also been seen to work on JRuby-9.4.2.0.)
* Its unit tests do not require a dedicated Mastodon instance and will
  work with just a scratch user account (and partially without even
  that).
* It's a bit more "batteries included" than the official gem tries to
  do more for you.
* It supports [Contexts](https://docs.joinmastodon.org/methods/statuses/#context).

Cons:

* It supports much less of the API than the offical gem.
* Its author thinks metaprogramming is cool.

## Some Quick Examples:

Here's the canonical use case, retrieving cat pictures:

    require 'shep'

    HOST = "... name of mastodon host goes here..."

    ss = Shep::Session.new(host: HOST)

    ss.each_tag_status('caturday', only_media: true, limit: 10) { |status|
      ss.fetch_status_with_media(status.id, media_dir = 'cat_pix')
    }

And, of course, uploading cat pictures:

    require 'shep'

    HOST = "... name of mastodon host goes here..."
    TOKEN = "...token goes here..."

    ss = Shep::Session.new(host: HOST, token: TOKEN)

    img_obj = ss.upload_media("cat.jpg",
                              content_type: 'image/jpeg',
                              description: "A cat.")

    post = ss.post_status("Here is a picture of a cat.",
                          spoiler_text: "cat pic",
                          visibility: :public,
                          media_ids: [img_obj.id])

    puts "Cat picture posted: #{post.url}"

It can also be used for other things.


## Installation

The usual way to install Shep is via gem:

    $ gem install shep

Alternately, you can fetch this repo and add it to your Ruby path.

If you wish to develop Shep, you'll also need Rake, YARD, and RSpec.


## Testing

The tests are somewhat clever in that if there isn't a Mastodon
instance available or if you don't have a writable account on it, it
will still try to run tests that don't need those things.

On they other hand, some of the tests will fail if (e.g.) an account
doesn't have enough statuses with attached pictures.  I've tried to
document this when it happens.  Usually, a test will fail rather than
let a part of the code go untested.

The tests get credentials from the environment.  Normally, I run
`rake test` from a script that first sets those variables to useful
values.

The file `run_rake_test.example.sh` documents these variables and what
they do.

There are also a number of tricky tests (e.g. for rate-limit-reached
cases) that need to be explicitly invoked using rspec tags.  See
`spec/session_zzz_tricky.rb` for details.


## License and Warranty

Shep is available under the terms of the GNU Affero GPL Version 3 plus
an exemption that allows you to release programs that use it under
your own terms.

It is offered **WITHOUT WARRANTY OF ANY KIND**.  If it breaks, you get to keep
the pieces.

While it is not legally required or enforcable, you are nontheless
honour-bound to use Shep only for good and never for evil.

