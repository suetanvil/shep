
#
# Exception classes and error checking code used by Shep
#



module Shep

  # Base class for exceptions originating from Shep.  Use this to trap
  # all Shep exceptions at once.
  #
  # Note that Shep usually does not trap exceptions thrown by
  # {Net::HTTP} and friends; you will need to trap those separately.
  class Error < RuntimeError; end

  # Error caused by using the interface incorrectly.  *Not* the same
  # as incorrect input data (usually)
  class Error::Caller < Error; end

  # Error caused by assigning a value of the wrong type to an
  # `Entity` field.
  class Error::Type < Error::Caller
    def initialize(boxdesc, got, *want)
      super("#{boxdesc} expects #{want.join(',')}; got #{got}")
    end
  end

  # Error caused by network issues.
  class Error::Remote < Error; end

  # Thrown when the server doesn't like what we did.  (Different
  # from Error::Remote because this assumes the server is working
  # correctly.)
  class Error::Server < Error; end

  # Thrown when the HTTP library returns an error response.
  #
  # Basically the same meaning as Error::Server but also includes
  # the response object for your perusal.
  class Error::Http < Error::Server
    attr_reader :response
    def initialize(response)
      msg = "HTTP Error #{response.class}"

      errmsg = find_error_msg_if_present(response)
      msg += ": #{errmsg}" if errmsg != ""

      super(msg)
      @response = response
    end

    private

    def find_error_msg_if_present(response)
      return "" unless response.class.body_permitted?
      obj = JSON.parse(response.body)
      return obj["error"] || ""
    rescue JSON::JSONError
      return ""
    end
  end

  # Thrown when the HTTP operation fails because a rate limit was
  # reached.  `Session.rate_limit` will **probably** return the
  # correct reset time, but no promises at this time.
  class Error::RateLimit < Error::Http ; end


  private

  # We use refinement to add 'assert' to object. This gets used a lot
  # for internal error checking.
  #
  # @private
  module Assert
    refine Object do
      def assert(msg = nil, &block)
        return if block.call
        msg = "Check failed" unless msg
        msg = block.source_location.join(':') + " - " + msg
        raise Error::Caller.new(msg)
      end
    end
  end
end
