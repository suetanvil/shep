
#
# Abstract base class for Entities.  As much as possible, all of the
# smarts are here.
#




module Shep
  using Assert


  # Abstract base class for Mastodon objects.
  #
  # Mastodon provides its content as JSON hashes with documented names
  # and values.  Shep takes this one step further and provides a class
  # for each object type.  These are similar to Ruby's `Struct` but
  # are also strongly typed.
  #
  # Typing is primarily useful for converting things that don't have
  # explicit JSON types (e.g. Time, URI) into Ruby types. However, it
  # will also catch the case where you're trying to set a field to
  # something with the wrong type.
  #
  # Supported types are:
  #
  # * Number    - (Integer but also allows Float)
  # * Boolean
  # * String
  # * URI       - (a Ruby URI object)
  # * Time      - parsed from and converted to ISO8601-format strings
  # * Entity    - an arbitrary Entity subclass
  # * Array     - strongly typed array of any of the above types
  #
  # Fields may also be set to nil, except for `Array` which must
  # instead be set to an ampty array.
  #
  # Entities can be converted to and from Ruby Hashes.  For this, we
  # provide two flavours of Hash: the regular Ruby Hash where values
  # are just the Ruby objects and the JSON hash where everything has
  # been converted to the types expected by a Mastodon server.
  #
  # For JSON hashes, `Time` objects become ISO8601-formatted strings,
  # `URI` objects become strings containing the url and `Entity`
  # subobjects become their own JSON hashes.  (Note that conversion to
  # JSON hashes isn't really used outside of some testing and internal
  # stuff so I don't guarantee that a Mastodon server or client will
  # accept them.)
  #
  # Normally, we care about initializing Entity objects from the
  # corresponding parsed JSON object and produce Ruby hashes when we
  # need to use a feature `Hash` provides.
  #
  # Subclasses are all defined inside the Entity namespace so that it
  # groups nicely in YARD docs (and because it makes the intent
  # obvious).
  class Entity

    # Default constructor; creates an empty instance.  You'll
    # probably want to use {with} or {from} instead.
    def initialize
      init_fields()
    end

    #
    # Instance creation
    #

    # Construct an instance initialized with Ruby objects.
    #
    # This intended for creating {Entity} subobjects in Ruby code.
    # Keys of {fields} must correspond to the class's supported fields
    # and be of the correct type.  No fields may be omitted.
    def self.with(**fields) =
      new.set_from_hash!(fields,
                         ignore_unknown: false,
                         from_json: false)

    # Construct an instance from the (parsed) JSON object returned by
    # Mastodon.
    #
    # Values must be of the expected types as they appear in the
    # Mastodon object (i.e. the JSON Hash described above).  Missing
    # key/value pairs are allowed and treated as nil; unknown keys are
    # ignored.
    def self.from(json_hash) =
      new.set_from_hash!(json_hash, ignore_unknown: true, from_json: true)


    # Set all fields from a hash.
    #
    # This is the back-end for {from} and {with}.
    #
    # @param some_hash      [Hash]  the Hash containing the contents
    #
    # @param ignore_unknown [Bool]  if false, unknown keys cause an error
    #
    # @param from_json      [Bool]  if true, expect values in the format
    #                               provided by the Mastodon API and convert
    #                               accordingly.  Otherwise, expect Ruby types.
    #
    def set_from_hash!(some_hash, ignore_unknown: false, from_json: false)
      some_hash.each do |key, value|
        key = key.intern
        unless has_fld?(key)
          raise Error::Caller.new("Unknown field: '#{key}'!") unless
            ignore_unknown
          next
        end

        if from_json
          getbox(key).set_from_json(value)
        else
          self.send("#{key}=".intern, value)
        end
      end

      return self
    end


    #
    # Printing
    #

    # Produce a **short** human-friendly description.
    #
    # @return [String]
    def to_s
      maxlen = 20
      notable = self.disp_fields()
        .reject{|fld| getbox(fld).get_for_json.to_s.empty? }
        .map{|fld|
          valtxt = getbox(fld).get
          valtxt = valtxt[0..maxlen] + '...' unless valtxt.size < maxlen+3
          "#{fld}=#{valtxt}"
        }
        .join(",")
      notable = "0x#{self.object_id.to_s(16)}" if notable.empty?

      "#{self.class}<#{notable}>"
    end
    alias inspect to_s

    protected

    def self.disp_fields = []

    public

    #
    # Basic access
    #

    # Retrieve a field value by name
    #
    # @param key [String,Symbol]
    #
    # @return [Object]
    def [](key) = getbox(key).get

    # Set a field value by name
    #
    # @param key    [String,Symbol]
    # @param value  [Object]
    #
    # @return [Object] the `value` parameter
    def []=(key, value)
      getbox(key).set(value)
      return value
    end

    # Compare for equality.
    #
    # Two Entity subinstances are identical if they are of the same
    # class and all of their field values are also equal according to
    # `:==`
    #
    # @return [Boolean]
    def ==(other)
      return false unless self.class == other.class
      keys.each{|k| return false unless self[k] == other[k] }
      return true
    end

    # Return a hash of the contents mapping field name to value.
    #
    # @param json_compatible [Boolean] if true, convert to JSON-friendly form
    #
    # If `json_compatible` is true, the resulting hash will be
    # easily convertable to Mastodon-format JSON.  See above.
    #
    # Unset (i.e. nil) values appear as entries with nil values.
    #
    # @return [Hash]
    def to_h(json_compatible = false)
      result = {}

      keys.each{|name|
        hkey = json_compatible ? name.to_s : name

        box = getbox(name)
        val = json_compatible ? box.get_for_json : box.get

        result[hkey] = val
      }

      result
    end

    # Produce a long-form human-friendly description of this Entity.
    #
    # This is mostly here for debugging.
    #
    # @return [String]
    def to_long_s(indent_level = 0)
      name_pad = keys.map(&:size).max + 2

      result = keys.map do |key|
        line = " " * (indent_level * 2)
        line += sprintf("%-*s", name_pad, "#{key}:")

        val = self[key]
        line += val.is_a?(Entity) ? val.to_long_s(indent_level + 1) : val.to_s
      end

      return result.join("\n")
    end

    # Wrapper around `puts to_long_s()`
    def print = puts(to_long_s)


    #
    # Subclass definition via cool metaprogramming
    #


    # Cool metaprogramming thing for defining {Entity} subclasses.
    #
    # A typical {Entity} subclass should contain only a call to this
    # method.  For example:
    #
    #       class Thingy < Entity
    #           fields(
    #               :id,        %i{show},       StringBox,
    #               :timestamp,                 TimeBox,
    #               :count,     %i{show},       NumBox,
    #               :url,                       URIBox,
    #           )
    #       end
    #
    # {fields} takes a variable sequence of arguments that must be
    # grouped as follows:
    #
    #   1. The field name. This **must** be a symbol.
    #
    #   2. An optional Array containing the symbol :show.  If given,
    #      this field will be included in the string returned by
    #      `to_s`.  (This is actually a mechanism for setting various
    #      properties, but all we need is `:show`, so that's it for
    #      now.)
    #
    #   3. The type specifier.  If omitted, defaults to StringBox.
    #
    #  The type specifier must be either:
    #
    #   1. One of the following classes: `TypeBox`, `StringBox`,
    #      `TimeBox`, `URIBox`, or `NumBox`, corresponding to the type
    #      this field will be.
    #
    #   2. A subclass of Entity, indicating that this field holds
    #      another Mastodon object.
    #
    #   3. An Array holding a single element which must be one of the
    #      above classes, indicating that the field holds an array of
    #      items of that type.
    #
    # @api private
    def self.fields(*flds)
      known_props = %i{show}.to_set

      names_and_types = []
      notables = []
      until flds.empty?
        name = flds.shift
        assert{ name.class == Symbol }

        properties = Set.new
        if flds[0].is_a?(Array) && flds[0][0].is_a?(Symbol)
          properties += flds[0]

          assert("Unknown properti(es): #{(properties - known_props).to_a}") {
            (properties - known_props).empty?
          }

          flds.shift
        end

        notables.push(name) if properties.include? :show

        if flds[0] && flds[0].class != Symbol
          typefld = flds.shift

          # Array means ArrayBox with the element as type
          if typefld.is_a? Array
            assert{typefld.size == 1 && typefld[0].is_a?(Class)}
            atype = typefld[0]

            # If this is an array of entity boxes, handle that
            atype = EntityBox.wrapping(atype) if atype < Entity

            type = ArrayBox.wrapping(atype)

          elsif typefld.is_a?(Class) && typefld < Entity
            type = EntityBox.wrapping(typefld)

          elsif typefld.is_a?(Class) && typefld < TypeBox
            type = typefld

          else
            raise Error::Caller.new("Unknown field type '#{typefld}'")
          end
        else
          type = StringBox
        end

        add_fld(name, type)
        names_and_types.push [name, type]
      end

      names_and_types.freeze
      notables.freeze

      add_init(names_and_types)
      make_has_name(names_and_types)
      make_disp_fields(notables)
      make_keys(names_and_types)

      # This gets used to generate documentation so we make it private
      # and (ab)use Object.send to call it later
      define_singleton_method(:names_and_types) { names_and_types }
      singleton_class.send(:private, :names_and_types)
    end


    private

    def self.make_keys(names_and_types)
      keys = names_and_types.map{|n, t| n}.freeze
      define_method(:keys) { keys }
    end

    def self.make_has_name(names_and_types)
      names_set = names_and_types.map{|n, t| n}.to_set.freeze
      define_method(:has_fld?) {|name| return names_set.include?(name) }
    end

    def self.make_disp_fields(notables)
      define_method(:disp_fields) { notables }
      protected(:disp_fields)
    end

    def self.add_init(names_and_types)
      define_method(:init_fields) {
        for name, box_type in names_and_types
          box = box_type.new("#{self.class}.#{name}")
          instance_variable_set("@#{name}".intern, box)
        end
      }
    end

    def self.add_fld(name, type)
      self.define_method(name) { getbox(name).get }
      self.define_method("#{name}=".intern) { |val| getbox(name).set(val) }
    end

    protected

    def getbox(name) = instance_variable_get("@#{name}".intern)
  end
end
