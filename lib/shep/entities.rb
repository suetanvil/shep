
#
# Definitions of all of the Mastodon entities
# 



module Shep
  using Assert

  # @see https://docs.joinmastodon.org/entities/CustomEmoji/
  class Entity::CustomEmoji < Entity
    fields(
      :shortcode,       %i{show},
      :url,                                 URIBox,
      :static_url,                          URIBox,
      :visible_in_picker,                   BoolBox,
      :category,        %i{show}
    )
  end

  # @see https://docs.joinmastodon.org/entities/Account/
  class Entity::Account < Entity
    fields(
      :id,              %i{show},
      :username,        %i{show},
      :acct,
      :display_name,    %i{show},
      :locked,                              BoolBox,
      :bot,                                 BoolBox,
      :created_at,                          TimeBox,
      :followers_count,                     NumBox,
      :following_count,                     NumBox,
      :statuses_count,                      NumBox,
      :note,
      :url,                                 URIBox,
      :avatar,                              URIBox,
      :avatar_static,                       URIBox,
      :header,                              URIBox,
      :header_static,                       URIBox
    )
  end

  # @see https://docs.joinmastodon.org/entities/Account/
  class Entity::MediaAttachment < Entity
    fields(
      :id,                  %i{show},
      :type,                %i{show},
      :url,                                 URIBox,
      :preview_uri,                         URIBox,
      :remote_url,                          URIBox,
      # :meta,
      :description,         %i{show},
      :blurhash,
    )
  end

  # @see https://docs.joinmastodon.org/entities/Status/#Mention
  class Entity::Status_Mention < Entity
    fields(
      :id,              %i{show},
      :username,        %i{show},
      :url,                                 URIBox,
      :acct,
    )
  end

  # @see https://docs.joinmastodon.org/entities/Status/#Tag
  class Entity::Status_Tag < Entity
    fields(
      :name,            %i{show},
      :url,                                 URIBox,
    )
  end

  # The specification describes this as a Hash object with arbitrary
  # fields.  `Shep` doesn't really support that (yet) so we only give
  # you the expected fields.
  #
  # @see https://docs.joinmastodon.org/entities/Status/#application
  class Entity::Status_Application < Entity
    fields(
      :name,            %i{show},
      :website,                             URIBox,
    )
  end

  # @see https://docs.joinmastodon.org/entities/Status/
  class Entity::Status < Entity
    fields(
      :id,              %i{show},
      :uri,                                 URIBox,
      :created_at,                          TimeBox,
      :account,                             Entity::Account,
      :content,         %i{show},
      :visibility,
      :sensitive,                           BoolBox,
      :spoiler_text,

      :media_attachments,                   [Entity::MediaAttachment],

      :applications,                        Entity::Status_Application,

      :mentions,                            [Entity::Status_Mention],
      :tags,                                [Entity::Status_Tag],

      :emojis,                              [Entity::CustomEmoji],

      :reblogs_count,                       NumBox,
      :favourites_count,                    NumBox,
      :replies_count,                       NumBox,

      :url,                                 URIBox,

      :in_reply_to_id,
      :in_reply_to_account_id,

      :reblog,                              Entity::Status,

      :language,
      :text,

      :edited_at,                           TimeBox,

      # Optional; nil unless the client is authorized
      :favourited,                          BoolBox,
      :reblogged,                           BoolBox,
      :muted,                               BoolBox,
      :bookmarked,                          BoolBox,
      :pinned,                              BoolBox,

      #:filtered
    )
  end

  # @see https://docs.joinmastodon.org/entities/Context/
  class Entity::Context < Entity
    fields(
      :ancestors,                           [Entity::Status],
      :descendants,                         [Entity::Status]
    )
  end

  # @see https://docs.joinmastodon.org/entities/StatusSource/
  class Entity::StatusSource < Entity
    fields(
      :id,                  %i{show},
      :text,
      :spoiler_text
    )
  end

  # @see https://docs.joinmastodon.org/entities/Notification/
  class Entity::Notification < Entity
    fields(
      :id,                  %i{show},
      :type,                %i{show},
      :created_at,                          TimeBox,
      :account,             %i{show},       Entity::Account,
      :status,                              Entity::Status,
      #:report
    )
  end
end
