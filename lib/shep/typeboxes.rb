

#
# This file is completely internal and so is not processed by yard.
#
# The "box" class heirarchy for Entity fields.  These are used to
# specify the types of Entity fields.  Specifically:
#
# 1. They enforce the expected types when assigning to the field.
#
# 2. They intelligently convert between Ruby types and Mastodon object
#    JSON types.  (e.g. Time <-> a string containing an ISO8601 date).
#
# 3. They are used to document the expected type.
#



module Shep
  using Assert

  private

  # Abstract base classes for all type boxes.  All scalar subclasses
  # may also hold nil.
  class TypeBox
    def initialize(field_desc)
      @desc = field_desc
      @value = nil
    end

    def ==(other) = (other.class == self.class && other.get == @value)

    def to_s = "#{self.class}(#{@value})"
    def inspect = to_s

    # Name for the Ruby type this holds as used by YARD.  (This is
    # part of making Entity self-documenting.)
    def self.to_yard_s = self.to_s.split(/::/)[-1].gsub(/Box$/, '')

    # Assignment from Ruby types with a type check.
    def set(newval)
      check(newval) unless newval == nil
      @value = newval
    end

    def unset? = (@value == nil)
    def set? = !unset?

    def get = @value

    # Set/Get in a format suitable for converting to JSON.
    def get_for_json = @value
    def set_from_json(newval) = set(newval)

    protected

    # Assert that 'new' is of one of the types in 'types'; raise an
    # Error::Type if it is not.
    def assert_type(new, *types)
      raise Error::Type.new(@desc, new.class, *types) unless
        types.include? new.class
    end
  end


  class StringBox < TypeBox
    def check(v) = assert_type(v, String)
  end

  class BoolBox < TypeBox
    def self.to_yard_s = "Boolean"
    def get_for_json = @value
    def check(v) = assert_type(v, TrueClass, FalseClass)
  end

  class TimeBox < TypeBox
    def check(v) = assert_type(v, Time)
    def set_from_json(v)
      set(Time.iso8601(v)) if v
    end
    def get_for_json = set? ? @value.utc.iso8601(6) : nil
  end

  class NumBox < TypeBox
    def self.to_yard_s = "Integer"   # a lie, but convenient
    def get_for_json = @value
    def check(v) = assert_type(v, Integer, Float)
  end

  class URIBox < TypeBox
    def check(v)
      raise Error::Type.new(@desc, v, URI::Generic) unless v.is_a?(URI::Generic)
    end
    def get_for_json = unset? ? @value : @value.to_s
    def set_from_json(v) = set( (v == nil) ? v : URI(v) )
  end

  # Abstract base class for TypeBox subinstances that hold Entities
  # (e.g. the Entity::Account field in a Status).
  class OnDemandTypeBox < TypeBox
    protected

    def element_klass() raise Error.new("Used base class!") ; end

    public

    def self.wrapping(type)
      @subklasses ||= {}

      tid = type.__id__ # we use __id__ to ensure we're hashing by identity

      unless @subklasses.has_key?(tid)
        klassname = "#{self}[#{type}]"

        sk = Class.new(self)
        sk.define_singleton_method(:element_klass) { return type }
        sk.define_singleton_method(:to_s) { return klassname }
        sk.define_singleton_method(:inspect) { return klassname }
        @subklasses[tid] = sk
      end

      return @subklasses[tid]
    end

    def element_klass = self.class.element_klass
  end


  class ArrayBox < OnDemandTypeBox
    def initialize(field_desc)
      super(field_desc)
      element_klass     # fails unless this is a subclass
    end

    def self.to_yard_s = "Array<#{self.element_klass.to_yard_s}>"
    
    def get = basic_get(false)
    def set(new_vals) = basic_set(new_vals, false)

    def get_for_json = basic_get(true)
    def set_from_json(new_vals) = basic_set(new_vals, true)

    private

    def basic_get(is_json)
      return [].freeze unless @value
      msg = is_json ? :get_for_json : :get
      return @value.map{|valbox| valbox.send(msg) }.freeze
    end

    def basic_set(new_vals, is_json)
      assert_type(new_vals, Array)
      msg = is_json ? :set_from_json : :set
      @value = new_vals.map{|item|
        self.element_klass
          .new("#{@field_desc}[#{@element_box_klass}]")
          .then { |box|
            box.send(msg, item)
            next box
          }
      }
      return new_vals
    end
  end

  class EntityBox < OnDemandTypeBox
    def check(v) = assert_type(v, self.element_klass)

    def self.to_yard_s = self.element_klass.to_s.gsub(/^Shep::/,'')

    def get_for_json = @value ? @value.to_h(true) : @value

    def set_from_json(jhash)
      return set(nil) if jhash == nil
      set(self.element_klass.from(jhash))
    end
  end

end
