
require 'net/https'

require 'uri'
require 'time'
require 'json'
require 'set'
require 'logger'
require 'securerandom'
require 'fileutils'

# SHEP: Simple Http intErface to Pmastodon
module Shep
end

require_relative 'shep/version'
require_relative 'shep/exceptions'
require_relative 'shep/typeboxes'
require_relative 'shep/entity_base'
require_relative 'shep/entities'
require_relative 'shep/session'

