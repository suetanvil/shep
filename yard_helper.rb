
require_relative "lib/shep.rb"

class EntityFieldDeclHandler < YARD::Handlers::Ruby::Base  #AttributeHandler
  handles method_call(:fields)
  namespace_only

  process do
    sanitized = {
      '<' => '&lt;',
      '>' => '&gt;',
    }

    mastotype = namespace.name.to_s.gsub(/_/, '::')
    new_docstr = "Ruby object representing a Mastodon `#{mastotype}` object:\n\n"

    nnt = Object.const_get(namespace.path).send(:names_and_types)
    nnt.each do |name, type|
      # We generate the argument list in Markdown because I am
      # completely done fighting with YARD to get it to recognize my
      # 'attr' tags.

      typename = type.to_yard_s
        .gsub(/[<>]/) {|match| sanitized[match]}    # Excape angle brackets
        .gsub(/(Entity::\w+)/, '{\1}')              # So YARD can generate refs

      new_docstr += " * #{name} (#{typename})\n"
    end

    new_docstr += "\n"
    new_docstr += "See `Shep::Entity` for an overview.\n\n"
    new_docstr += namespace.docstring.to_raw.strip

    namespace.docstring.replace(new_docstr)
  end
end
