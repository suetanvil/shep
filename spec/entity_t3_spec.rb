
require_relative '../lib/shep.rb'
require_relative 'spec_helper.rb'

include Shep

describe Entity, :offline do
  it "real-world subclasses initialize from real-world JSON." do
    entities = [Entity::Account, Entity::Status]

    puts "" # newline so the filenames are nicely aligned
    
    sample_dir = File.join(File.dirname(__FILE__), 'json_objects')
    for ek in entities
      name = ek.to_s.downcase.split('::')[-1]
      
      for sample in Dir.glob(File.join(sample_dir, "#{name}.*.json"))
        puts "read: #{File.basename(sample)}"
        
        json = open(sample, "r") { |fh| JSON.parse( fh.read ) }
        entity = ek.from(json)

        json2 = JSON.generate(entity.to_h(true))    # probalby overkill
        entity2 = ek.from(JSON.parse(json2))

        expect( entity2 ) .to eq entity
      end
    end
  end
end
