
# An arbitrary time in the past
Then = Time.new(2021,02,06,22,11,52,'Z')


# Test entity One (basic Entity)

TE_KEYS =  %i{id uri time num bool}

class TestEntity < Entity
  fields(
    :id,        %i{show},                           # visible string
    :uri,       %i{show},           URIBox,         # uri field
    :time,                          TimeBox,        # time field
    :num,                           NumBox,         # numeric field
    :bool,                          BoolBox,        # bool field
  )
end

def new_te
  return TestEntity.with(
    id:   "123",
    uri:  URI("http://example.com"),
    time: Then,
    num: 42,
    bool: false
  )
end


#
# Test Entity Two (Entity with array fields)
#

class TestEntity2 < Entity
  fields(
    :id,        %i{show},           NumBox,
    :names,                         [StringBox],
    :dates,                         [TimeBox],
  )
end

def new_te2
  return TestEntity2.with(
    id: 42,
    names: ["Alice", "Bob", "Christa", "Daniel"],
    dates: [Then, Then+1, Then+2],
  )
end


#
# Test Entity Three (Entity with another Entity as field type)
# 

class TestEntity3 < Entity
  fields(
    :thing,     %i{show},
    :nested,                        TestEntity2,
  )
end

def new_te3
  return TestEntity3.with(
    thing: "some text",
    nested: new_te2,
  )
end

TE3_JSON_TXT = <<EOF
{
  "thing": "some text",
  "nested": {
    "id": 42,
    "names": [
      "Alice",
      "Bob",
      "Christa",
      "Daniel"
    ],
    "dates": [
      "2021-02-06T22:11:52Z",
      "2021-02-06T22:11:53Z",
      "2021-02-06T22:11:54Z"
    ]
  }
}
EOF


#
# Test Entity with Array of Entities
# 

class TestEntity_ArrayOfNested < Entity
  fields(
    :boring,    %i{show},           NumBox,
    :na,                            [TestEntity3],
  )
end

def new_te_aon
  te3 = new_te3
  
  te3a = new_te3

  te2a = new_te2
  te2a.names = ["Emma", "Frank", "Georgia"]

  te3a.nested = te2a
  
  return TestEntity_ArrayOfNested.with(
    boring: 1,
    na: [te3, te3a],
  )
end




