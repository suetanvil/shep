
$: << 'lib'

require 'tmpdir'

# The Mastodon instance to use for online testing
HOST                    = ENV['SHEP_TEST_HOST']

# Public account on HOST to use for unauthenticated feature testing
READ_ACCOUNT_HANDLE     = ENV['SHEP_READ_ACCOUNT_HANDLE']
READ_ACCOUNT            = ENV['SHEP_READ_ACCOUNT']

# Account details on HOST to use for authenticated/write feature
# testing
TOKEN                   = ENV['SHEP_TOKEN']
WRITE_ACCOUNT           = ENV['SHEP_ACCOUNT']

# If set and everything else is in order, we also run the rate-limited
# tests.
ALL_TESTS_REQUESTED     = !!ENV['SHEP_TEST_ALL']



# UUID to put in test posts so we can identify them when testing deletion.
POST_UUID = "fc49e486-d445-4ca7-9d6d-a27c89c5c471"  #SecureRandom.uuid
# Currently a constant so that I can clean up after broken tests.
# Ideally, this should be different for each run.


# Tags:
#
#   offline         - runs without a Mastodon server (always on)
#   anonymous       - requires a server but no login
#   account         - requires normal server account (which it *may* modify!)
#   account_limited - like 'account' but will also perform heavily
#                     rate-limited actions (e.g. delete, upload media)
#
#


RSpec.configure {|conf|
  if conf.inclusion_filter.empty?
    allow_anonymous     = HOST && READ_ACCOUNT && READ_ACCOUNT_HANDLE
    allow_account       = allow_anonymous && TOKEN && WRITE_ACCOUNT
    allow_limited       = allow_account && ALL_TESTS_REQUESTED

    if ALL_TESTS_REQUESTED && !allow_limited
      puts "Requested all tests but some parameters are missing."
      # I suppose I should say which...
      exit 1
    end

    puts "Test groups chosen:"
    if allow_limited
      puts "  * all tests"
    else
      puts "  * offline tests"
      puts "  * anonymous online tests"         if allow_anonymous
      puts "  * authenticated online test"      if allow_account
    end
    puts

    conf.filter_run_including(:offline)
    conf.filter_run_including(:anonymous)       if allow_anonymous
    conf.filter_run_including(:account)         if allow_account
    conf.filter_run_including(:account_limited) if allow_limited
  end

  conf.default_formatter = 'doc'
}

# stolen from https://gist.github.com/sue445/d840d6f68771a14a48fc
shared_context :uses_temp_dir do
  around do |example|
    Dir.mktmpdir("rspec-") do |dir|
      @tmp_dir = dir
      example.run
    end
  end

  attr_reader :tmp_dir
end
