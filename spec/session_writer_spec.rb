
require 'fileutils'

require_relative 'spec_helper'
require_relative '../lib/shep'

include Shep

describe Session, :account_limited do
  include_context :uses_temp_dir

  before(:all) {
    @new_posts = []
  }

  it "posts statuses" do
    testpost = "Test post #{POST_UUID} "

    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    visibilities = %w{public unlisted private direct}
    for vis in visibilities
      post = ss.post_status(testpost + Time.now.to_s,
                            visibility: vis,
                            language: "en")
      @new_posts.push post

      expect( visibilities.index(post.visibility) )
        .to be >= visibilities.index(vis)

      retrieved_post = ss.fetch_status(post.id)
      expect( retrieved_post ) .to eq post
    end

    spoiler_text = "is this a test post?"
    post = ss.post_status(testpost + Time.now.to_s,
                          spoiler_text: spoiler_text)
    @new_posts.push post
    expect( post.spoiler_text ) .to eq spoiler_text
    expect( post.sensitive ) .to be true

    retrieved_post = ss.fetch_status(post.id)
    expect( retrieved_post ) .to eq post
  end

  it "posts status with media" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    file1 = File.join(File.dirname(__FILE__), "data", "smallimg.jpg")
    file2 = File.join(File.dirname(__FILE__), "data", "smallish.jpg")

    img1 = ss.upload_media(file1, content_type: 'image/jpeg')
    expect( img1.type ) .to eq 'image'

    desc = "IMAGE_TEXT!!!!"
    img2 = ss.upload_media(file2,
                           description: desc,
                           focus_x: 0.0,
                           focus_y: 0.7)
    expect( img2.type ) .to eq 'image'
    expect( img2.description ) .to eq  desc

    msg = "Upload unit test post; #{Time.now.iso8601}; #{POST_UUID}"
    post = ss.post_status(msg, media_ids: [img1.id, img2.id])
    @new_posts.push(post)   # so it gets deleted later

    post2, files = ss.fetch_status_with_media(post.id, tmp_dir())
    expect( post2.media_attachments[0].id ) .to eq img1.id
    expect( post2.media_attachments[1].id ) .to eq img2.id

    # It turns out that Mastodon modifies the image files so that what
    # we get back isn't bit-identical to what we uploaded.  Since
    # there's no bulletproof way to extract stuff like extent from the
    # image, we settle for just confirming that we get the correct
    # number of onon-empty files.
    expect( files.size ) .to eq 2
    for path in files.values
      expect( File.stat(path).size ) .to be > 1000
    end
  end

  it "edits statuses" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    base_text = "edited msg #{POST_UUID}\n#{Time.now}\n-"
    post = ss.post_status(base_text, language: "en")
    @new_posts.push(post)   # delete it during the deletion test

    expect( ss.fetch_status(post.id) ) .to eq post
    expect( post.content )      .to include POST_UUID
    expect( post.sensitive )    .to be false
    expect( post.spoiler_text ) .to eq ""
    expect( post.language )     .to eq "en"

    src = ss.fetch_status_src(post.id)
    expect(src.text) .to eq base_text

    # Edit to update the text.
    new_text = base_text + "!!!!"
    post2 = ss.edit_status(post.id, new_text)
    expect( ss.fetch_status(post.id) ) .to eq post2

    expect( post2.content )      .to include '!!!'
    expect( post2.sensitive )    .to be false
    expect( post2.spoiler_text ) .to eq ""
    expect( post2.language )     .to eq "en"

    # Edit to change language, spoiler and sensitive
    post2 = ss.edit_status(post.id, new_text,
                           spoiler_text: "spoiler",
                           language: "fr")
    expect( ss.fetch_status(post.id) ) .to eq post2

    expect( post2.content )      .to include '!!!'
    expect( post2.sensitive )    .to be true
    expect( post2.spoiler_text ) .to eq "spoiler"
    expect( post2.language )     .to eq "fr"

    # Edit to revert sensitive and language
    post2 = ss.edit_status(post.id, new_text)
    expect( ss.fetch_status(post.id) ) .to eq post2

    expect( post2.content )      .to include '!!!'
    expect( post2.sensitive )    .to be false
    expect( post2.spoiler_text ) .to eq ""
    expect( post2.language )     .to eq "en"

    # Edit to add a media attachment
    file1 = File.join(File.dirname(__FILE__), "data", "smallimg.jpg")
    img1 = ss.upload_media(file1, content_type: 'image/jpeg')

    new_text2 = new_text + "\n!PIX!\n"
    post2 = ss.edit_status(post.id, new_text2,
                           spoiler_text: "pix",
                           media_ids: [img1.id],
                          )
    expect( ss.fetch_status(post.id) ) .to eq post2

    expect( post2.content )      .to include '!PIX!'
    expect( post2.sensitive )    .to be true
    expect( post2.spoiler_text ) .to eq "pix"
    expect( post2.language )     .to eq "en"
    expect( post2.media_attachments.map{|ma| ma.id} )
      .to eq [img1.id]

    # Edit to remove the media attachment
    post2 = ss.edit_status(post.id, new_text2,
                           spoiler_text: "pix",
                           media_ids: [],
                          )
    expect( ss.fetch_status(post.id) ) .to eq post2

    expect( post2.content )      .to include '!PIX!'
    expect( post2.sensitive )    .to be true
    expect( post2.spoiler_text ) .to eq "pix"
    expect( post2.language )     .to eq "en"
    expect( post2.media_attachments ) .to eq []
  end

  it "deletes statuses" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    # Test for the test itself
    expect( @new_posts.size ) .to be > 0

    # Fun fact: a Mastodon instance limits deletions to 30/hour, so
    # running this test a few times will completely eat through that.
    @new_posts.each do |status|
      del_post = ss.delete_status(status.id)
      expect( del_post.id ) .to eq status.id
      expect( del_post.text.include? POST_UUID ) .to be true
    end

    @new_posts = []
  end
end
