
require_relative '../lib/shep.rb'
require_relative 'spec_helper.rb'

include Shep

require_relative "entity_common.rb"


describe Entity, :offline do
  it "provides array access" do
    te = new_te2

    expect( te.names ) .to eq ["Alice", "Bob", "Christa", "Daniel"]

    te.names = ["Eve", "Frank", "Georgia"]
    expect( te.names ) .to eq ["Eve", "Frank", "Georgia"]
  end

  it "enforces array element types" do
    te = new_te2

    nn = ["Alice", nil, "Christa"]
    te.names = nn
    expect( te.names ) .to eq nn

    expect{ te.names = ["a", 1, "2"] } .to raise_error(Shep::Error::Type)
    expect{ te.names = nil }  .to raise_error(Shep::Error::Type)

    te.names = []
    expect( te.names )  .to eq []
  end

  it "produces json arrays from array items" do
    te = new_te2
    tej = te.to_h(true)
    expect( tej["names"] ) .to eq ["Alice", "Bob", "Christa", "Daniel"]

    dates = tej["dates"].map{|datestr|
      expect(datestr.class) .to be String
      Time.iso8601(datestr)
    }

    expect( dates ) .to eq te.dates
  end
  
  it "produces ruby hashes as usual" do
    te = new_te2
    tej = te.to_h(false)
    expect( tej[:names] ) .to eq ["Alice", "Bob", "Christa", "Daniel"]
    expect( tej[:dates] ) .to eq te.dates
  end

  it "supports nested entities" do
    te2 = new_te2
    te3 = new_te3
    expect( te3.nested ) .to eq te2

    te2a = new_te2
    te2a.id = 12345;
    te2a.names = ["Stan", "Ollie"]

    te3.nested = te2a
    expect( te3.nested ) .to eq te2a

    expect{ te3.nested = new_te3 } .to raise_error(Shep::Error::Type)
    expect{ te3.nested = "some string" } .to raise_error(Shep::Error::Type)
  end

  it "(with nested Entity) produces and can be created from Ruby hash" do
    te3 = new_te3
    h = te3.to_h

    te3a = TestEntity3.new
    te3a.set_from_hash!(h)

    expect( te3a ) .to eq te3
  end

  it "(with nested entity) can be initialized from JSON" do
    te3_json = JSON.parse(TE3_JSON_TXT)
    te3 = TestEntity3.from(te3_json)

    te3a = new_te3
    expect( te3 ) .to eq te3a
  end

  it "produces JSON hashes" do
    te3 = new_te3
    te3_jsonable_hash = te3.to_h(true)
    te3_json_str = JSON.pretty_generate(te3_jsonable_hash)

    te3a_jsonable_hash = JSON.parse(te3_json_str)
    te3a = TestEntity3.from(te3a_jsonable_hash)

    expect( te3a ) .to eq te3

    te3a_js = JSON.pretty_generate( te3a.to_h(true) )
    expect( te3a_js ) .to eq te3_json_str
  end

  it "with arrays of nested entities" do
    tea = new_te_aon

    expect( tea.na[0] ) .to eq new_te3

    tea_h = tea.to_h(true)
    tea2 = TestEntity_ArrayOfNested.from(tea_h)
    expect( tea2 ) .to eq tea
  end

  it "supports nil values for nested entities" do
    te3 = new_te3
    te3.nested = nil
    expect( te3.nested ) .to be nil

    te3a_json = JSON.parse( '{"thing": "some text"}' )
    te3a = TestEntity3.from(te3a_json)
    expect( te3a.nested ) .to be nil
  end


end
