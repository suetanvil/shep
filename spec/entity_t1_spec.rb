
require_relative '../lib/shep.rb'
require_relative 'spec_helper.rb'

include Shep

require_relative "entity_common.rb"

describe Entity, :offline do
  it "can be initialized via keyword args" do
    te = new_te()

    expect( te.id )     .to eq "123"
    expect( te.uri )    .to eq URI("http://example.com")
    expect( te.time )   .to eq Then
    expect( te.num )    .to eq 42
    expect( te.bool )   .to eq false

  end

  it "does basic hash-like stuff" do
    te = new_te()
    expect( te.keys.to_set ) .to eq TE_KEYS.to_set

    newvals = ["456", URI("http://bogus.com"), Time.now, 999, true]
    TE_KEYS.zip(newvals).each {|k, v|
      expect( te[k] ) .to eq te.send(k)
      expect( te[k] ) .to_not eq v

      te[k] = v
      expect( te[k] ) .to eq v
    }
  end

  # it "tests for completeness" do
  #   te = new_te()
  #   expect( te.complete? ) .to be true

  #   te2 = TestEntity.with(id: '999')
  #   expect( te2.complete? ) .to be false
  # end
  
  it "tests for equality" do
    te = new_te()
    te2 = new_te()

    expect( te == te2 ) .to be true
    expect( te2 == te ) .to be true

    te.id = "something else"
    expect( te == te2 ) .to be false
    expect( te2 == te ) .to be false

  end

  it "does struct-like access" do
    te = new_te()
    expect( te.id )     .to be te[:id]
    expect( te.uri )    .to be te[:uri]
    expect( te.time )   .to be te[:time]
    expect( te.num )    .to be te[:num]
    expect( te.bool )   .to be te[:bool]

    tmp = "xxx"
    te.id = tmp
    expect( te.id ) .to eq tmp

    tmp = URI("http://zombo.com")
    te.uri = tmp
    expect( te.uri ) .to eq tmp

    tmp = Then - 100000
    te.time = tmp
    expect( te.time ) .to eq tmp

    tmp = 999
    te.num = tmp
    expect( te.num ) .to eq 999

    tmp = true
    te.bool = true
    expect( te.bool ) .to eq tmp
  end

  it "enforces field types" do
    te = new_te()

    types = {
      id: "hello",
      uri: URI("http://bogus.com"),
      time: Then - 1000000,
      num: 123,
      bool: true
    }

    count = 0
    for fld in types.keys
      expect( te[fld] ) .to_not eq types[fld]
      te[fld] = types[fld]
      expect( te[fld] ) .to eq types[fld]

      types.keys.each{|other_fld|
        next if fld == other_fld

        expect{ te[fld] = types[other_fld] }
          .to raise_error(Shep::Error::Type)
        expect{ te.send("#{fld}=".intern, types[other_fld]) }
          .to raise_error(Shep::Error::Type)

        count += 1
      }
    end

    # Sanity check to ensure that the above loops got evaluated.
    expect( count ) .to eq(types.size * (types.size - 1))
  end

  it "allows null values" do
    te = new_te()

    TE_KEYS.each{|key|
      prev = te[key]
      expect( prev ) .to_not eq nil
      te[key] = nil
      expect( te[key] ) .to eq nil

      te[key] = prev
      expect( te[key] ) .to eq prev
    }
  end

  it "can be initialized from Mastodon JSON entities" do
    jv = '{"id":"123","uri":"http://example.com",' +
      '"time":"2021-02-06T22:11:52Z","num":42, "bool":false}'
    te = TestEntity.from(JSON.parse(jv))

    ref_te = new_te
    ref_te.keys.each{|k|
      expect( te[k] ) .to eq ref_te[k]
    }
  end
  
  it "implements a to_h that provides JSON-able values" do
    jv = '{"id":"123","uri":"http://example.com",' +
      '"time":"2021-02-06T22:11:52.000000Z","num":42, "bool":false}'
    jvh = JSON.parse(jv)

    te = TestEntity.from(jvh)

    jvh2 = te.to_h(true)
    expect( jvh2 ) .to eq jvh
  end

  it "implements a to_h that also provides Ruby objects" do
    jv = '{"id":"123","uri":"http://example.com",' +
      '"time":"2021-02-06T22:11:52Z","num":42, "bool":false}'
    jvh = JSON.parse(jv)
    te = TestEntity.from(jvh)

    te_h = te.to_h

    expect( te_h[:id].class ) .to be String
    expect( te_h[:uri].is_a? URI::Generic ) .to be true
    expect( te_h[:time].is_a? Time ) .to be true
    expect( te_h[:num] ) .to eq 42
    expect( te_h[:bool] ) .to be false
  end
end
