
# Tests requiring authentication

require_relative 'spec_helper'
require_relative '../lib/shep'

include Shep

describe Session, :account do

  it "fetches status sources." do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")
    statuses = ss.each_status(WRITE_ACCOUNT, limit: 1).to_a
    expect( statuses.empty? ) .to be false
    stat0 = statuses[0]

    src = ss.fetch_status_src(stat0.id)

    words = src.text.split
    puts "Couldn't find a word in Entity::StatusSource" if words.empty?
    expect( words.empty? || stat0.content.include?(words[0]))
      .to be true
  end

  it "reads the home timeline" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")
    statuses = ss.each_home_status(limit: 10).to_a
    expect( statuses.size ) .to be > 0
  end

  it "reads notifications" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    ntfns = ss.each_notification(limit: 10).to_a
    expect( ntfns.size ) .to be > 0

    ntfns.each{|ntfn|
      ntfn2 = ss.fetch_notification(ntfn.id)
      expect( ntfn2 ) .to eq ntfn
    }
    first = ntfns[0]

    # Test 'types' filter
    proc {
      ntfns2 = ss
        .each_notification(limit: 10, types: [first.type])
        .to_a

      expect( ntfns2[0] ) .to eq first

      expect( ntfns2.select{|n| n.type == first.type} ) .to eq ntfns2
    }.call

    # Test 'exclude_types' filter
    proc {
      ntfns2 = ss
        .each_notification(limit: 10, exclude_types: [first.type])
        .to_a
      expect( ntfns2[0] ) .to_not eq first
      expect( ntfns2.map{|n| n.type}.include?(first.type) ) .to be false
    }.call

    # Test 'account_id' filter
    proc {
      ntfns2 = ss
        .each_notification(limit: 10, account_id: first.account.id)
        .to_a

      expect( ntfns2[0] ) .to eq first

      expect( ntfns2.select{|n| n.account.id == first.account.id} )
        .to eq ntfns2
    }.call
  end

  it "timeline retrivals support max_id:" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    range_fn = proc { |&action|
      group_1 = action.call(limit: 10).to_a
      expect(group_1.size) .to eq 10   # not an error, but we can't continue if false

      group_2 = action.call(limit: 5, max_id: group_1[4].id).to_a
      expect( group_2.size ) .to be > 0 # not an error but...

      expect( group_2 ) .to eq group_1[5..]
    }

    range_fn.call{|**kwargs| ss.each_status(READ_ACCOUNT, **kwargs)}
    range_fn.call{|**kwargs| ss.each_public_status(**kwargs)}
    range_fn.call{|**kwargs| ss.each_tag_status("caturday", **kwargs)}
    range_fn.call{|**kwargs| ss.each_home_status(**kwargs)}
  end
end
