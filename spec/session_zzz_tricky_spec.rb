
# These are the things that are hard or slow to test, so they need to
# be run by hand, possibly with some external intervention.


require_relative 'spec_helper'
require_relative '../lib/shep'

include Shep


# Test `dismiss_notification`; this permanently dismisses the most
# recent notification so you will need to first confirm that a)
# there's a notification present and b) it's one you don't care about.
describe Session, :tricky_del_notification do
  it "expects to be configured for writing" do
    expect(WRITE_ACCOUNT) .to_not be nil
  end

  it "dismisses notifications" do
    ss = Session.new(host: HOST, token: TOKEN, ua_comment: "(integration test)")

    # We dismiss the first notification, then confirm that it is no
    # longer there.  Since this is not repeatable, we skip it by
    # default and manually enable it when we want to test it.

    ntfns = ss.each_notification(limit: 10).to_a
    expect( ntfns.size ) .to be > 0
    first = ntfns[0]

    ss.dismiss_notification(first.id)

    ntfns2 = ss.each_notification(limit: 10).to_a
    expect( ntfns2.size ) .to eq ntfns.size - 1
    expect( ntfns2[0] ) .to_not eq first
  end
end


# These tests exhaust the ratelimit of the delete API endpoint several
# times.  Running these two tests takes a long time and leaves us with
# no more deletes.
describe Session, :tricky_ratelimit do

  it "expects to be configured for writing" do
    expect(WRITE_ACCOUNT) .to_not be nil
  end

  it "retries when the rate limit is reached" do
    # Enables retry mode and sets rlhook as the hook function to call,
    # then posts and deletes status until the hook as been called.
    # Afterward, we make one more try.

    rlhook_called = false
    rlhook = proc do |rl|
      rlhook_called = true

      delay = (rl.reset - Time.now).round
      STDOUT.puts "Waiting #{delay}s for rate limit to reset."

      # We stop at the 3 second mark so the underlying delay will also
      # kick in.
      while rl.reset - 3 >= Time.now
        STDOUT.print "\r    #{(rl.reset - Time.now).round} seconds     "
        STDOUT.flush
        sleep 1
      end

      STDOUT.puts "\nDone!"
    end

    ss = Session.new(host: HOST,
                     token: TOKEN,
                     ua_comment: "(integration test; rate limit reached)",

                     rate_limit_retry: true,
                     retry_hook: rlhook,

                     logger: :info
                    )

    post_and_del = proc do
      testpost = "test post for deleting9 #{POST_UUID} #{Time.now}"
      post = ss.post_status(testpost)
      delpost = ss.delete_status(post.id)

      expect( post.id ) .to eq delpost.id

      STDOUT.print "."
      STDOUT.flush
    end


    limit = nil
    count = 0
    while !rlhook_called
      post_and_del.call

      # First pass, we fetch the deletion limit
      limit = ss.rate_limit.limit unless limit

      count += 1
      expect( count ) .to be < (2*limit + 1)
    end

    # Ensure we did at least one iteration
    expect( count ) .to be >= 1
    expect( rlhook_called ) .to be true

    # And ensure that we can do another post/delete cycle
    post_and_del.call
    expect( ss.rate_limit.remaining ) .to be > 0
  end


  # This test will deliberately exhaust the deletion rate limit.
  it "throws Error::RateLimit when rate limit is exhusted" do
    # We post and delete the post until the ratelimit is exhausted, at
    # which point Session should throw a Shep::Error::RateLimit

    ss = Session.new(host: HOST,
                     token: TOKEN,
                     ua_comment: "(integration test; rate limit reached)")

    post_and_del = proc{
      testpost = "test post #{POST_UUID} #{Time.now}"
      post = ss.post_status(testpost)
      delpost = ss.delete_status(post.id)

      expect( post.id ) .to eq delpost.id
    }

    expect { 50.times{ post_and_del.call } }
      .to raise_error(Shep::Error::RateLimit)
  end
end


# Ratelimit retry is different for media uploads because the media
# file is an IO-like object that gets read as part of the upload.
# This test ensures that this also works.
#
# (It does this by uploading the same image file over and over again.
# It's a small file though, so hopefully that will minimize the stress
# we put on the server.)
describe Session, :tricky_ratelimit_media_upload do
  it "expects to be configured for writing" do
    expect(WRITE_ACCOUNT) .to_not be nil
  end

  it "rate-limit retry works with media uploads too" do

    rlhook_called = false
    rlhook = proc do |rl|
      rlhook_called = true

      delay = (rl.reset - Time.now).round
      STDOUT.puts "Waiting #{delay}s for rate limit to reset."

      # We stop at the 3 second mark so the underlying delay will also
      # kick in.
      while rl.reset - 3 >= Time.now
        STDOUT.print "\r    #{(rl.reset - Time.now).round} seconds     "
        STDOUT.flush
        sleep 1
      end

      STDOUT.puts "\nDone!"
    end

    ss = Session.new(host: HOST,
                     token: TOKEN,
                     ua_comment: "(integration test; rate limit reached)",

                     rate_limit_retry: true,
                     retry_hook: rlhook,

                     logger: :info
                    )

    media_file = File.join(File.dirname(__FILE__), "data", "smallimg.jpg")

    limit = nil
    count = 0
    while !rlhook_called
      ma = ss.upload_media(media_file)

      # First pass, we fetch the deletion limit
      limit = ss.rate_limit.limit unless limit

      STDOUT.print "."
      STDOUT.flush

      count += 1
    end

    # Ensure we did at least one iteration
    expect( rlhook_called ) .to be true
    expect( count ) .to be >= 1

    # And ensure that we can do another post/delete cycle
    ma = ss.upload_media(media_file)
    expect( ma.id ) .to_not be nil  # kind of redundant, but whatever
  end
end
